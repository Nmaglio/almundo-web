import { Component, Pipe, PipeTransform } from '@angular/core';

import { faStar,faAngleDown} from '@fortawesome/free-solid-svg-icons';
import { HotelService } from './../services/hotel.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  faStar = faStar;
  faAngleDown = faAngleDown;

  toggleName: any = false;
  toggleStars: any = false;

  starsFilter = [
    { checked: true, value: 0, name: 'Todas las Estrellas' },
    { checked: false, value: 5, name: '5 Estrellas' },
    { checked: false, value: 4, name: '4 Estrellas' },
    { checked: false, value: 3, name: '3 Estrellas' },
    { checked: false, value: 2, name: '2 Estrellas' },
    { checked: false, value: 1, name: '1 Estrella' }
  ];

  hotels = [];
  nameHotel:string = "";
  loading:boolean = true;

  constructor(private _hotelsService: HotelService) {}

  ngOnInit(): void {
    this.getHotels();
  }

  checkedStar(filter) {
    if (filter.value == 0) {
      this.starsFilter.forEach(f => { f.value != 0 ? f.checked = false : null; })
    } else {
      this.starsFilter.forEach(f => { f.value == 0 ? f.checked = false : null; })
    }
  }

  getHotels() {

    this._hotelsService.getHotels(this.nameHotel, this.starsFilter).then((res: any[]) => {
      this.hotels = res;
    }).catch(() => {
      //TODO: catch error
    }).finally( () => {
      this.loading = false;
    });
  }

  toggleClickName(): void {
    this.toggleName = !this.toggleName;
  }

  toggleClickStars(): void {
    this.toggleStars = !this.toggleStars;
  }

}


