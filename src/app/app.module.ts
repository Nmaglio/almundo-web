import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { HttpClientModule } from '@angular/common/http';

import { HotelService } from './../services/hotel.service';
import { HttpService } from './../services/http.service';
import { SafePipe } from 'src/pipes/safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
  BrowserModule,
    AppRoutingModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [
    HttpService,
    HotelService,
    SafePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
