import { Injectable } from '@angular/core';
import { HttpService } from './http.service';


@Injectable({
    providedIn: 'root'
})
export class HotelService {

    constructor(private http: HttpService) { }

    public getHotels(name:string, stars:object) {
        let params = { name , stars : JSON.stringify(stars) }
        return this.http.getRequest("/hotels", params).toPromise();
    }

}