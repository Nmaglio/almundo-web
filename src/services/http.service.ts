import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {
  private baseUrl = "/api";

  constructor(private http: HttpClient) { }

  public getRequest<T>(url: string, params?: object): Observable<T> {
    let urlParms = this.formarUrl(url, params);
    return this.http.get<T>(this.baseUrl + urlParms);
  }

  private formarUrl(url: string, params: object) {

    let tmpUrl = url;

    if (params != null || params != undefined) {
      tmpUrl = tmpUrl + '?';
      Object.keys(params).forEach((key) => {
        if (params[key] != null) {
          tmpUrl = tmpUrl + encodeURIComponent(key) + "=" + encodeURIComponent(params[key]) + "&";
        }
      });
      tmpUrl = tmpUrl.substring(0, tmpUrl.length - 1);
    }

    return tmpUrl;
  }
}
